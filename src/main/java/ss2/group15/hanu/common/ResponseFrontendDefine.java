package ss2.group15.hanu.common;



public class ResponseFrontendDefine {

	public static final int GENERAL = 999;
	// thành công
	public static final String CODE_SUCCESS = "0";

	public static final String CODE_FAIL= "1";

	// exception không tìm thấy entity
	public static final int CODE_NOT_FOUND = 2;
	public static final int BAD_REQUEST_PARAMS=8;

}
