pipeline {

    agent any

	tools {
        maven "maven3"
    }

    environment {
        registry = "thienpvt/backend"
        registryCredential = 'dockerCredential'
    }

    stages{
        stage("build & SonarQube analysis") {
            steps {
                script {
                    def mvnCommand = (env.BUILD_NUMBER == "1") ? "mvn clean install sonar:sonar -DskipTests" : "mvn package sonar:sonar -DskipTests"
                    withSonarQubeEnv('sonar-pro') {
                        sh mvnCommand
                    }
                }
            }
            post {
                success {
                    echo 'Now Archiving...'
                    archiveArtifacts artifacts: '**/target/*.jar'
                }
            }
        }

        stage("Quality Gate") {
            steps {
                timeout(time: 1, unit: 'HOURS') {
                    waitForQualityGate abortPipeline: true
                }
            }
        }

        stage ('CODE ANALYSIS WITH CHECKSTYLE'){
            steps {
                sh 'mvn checkstyle:checkstyle'
            }
            post {
                success {
                    echo 'Generated Analysis Result'
                }
            }
        }


        stage('Building image') {
            steps{
              script {
                dockerImage = docker.build registry + ":$BUILD_NUMBER"
              }
            }
        }

        stage('Deploy Image') {
          steps{
            script {
              docker.withRegistry( '', registryCredential ) {
                dockerImage.push("$BUILD_NUMBER")
                dockerImage.push('latest')
              }
            }
          }
        }

        stage('Remove Unused docker image') {
          steps{
            sh "docker rmi $registry:$BUILD_NUMBER"
          }
        }

        stage('Kubernetes Deploy') {
          agent { label 'KOPS' }
          steps {
             sh "helm upgrade --install --force app-stack helm/mycharts --set beimage=${registry}:${BUILD_NUMBER} --namespace prod"
          }
        }

    }


}